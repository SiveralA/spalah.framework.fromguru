<?php

namespace models;

use core\Application;

class User
{
    public $id;
    
    public $name;
    
    public $email;
    
    public $password;
    
    public $created_at;

    public static function getEntityName()
    {
        return 'user';
    }

    public static function getOne($criteria = [],$limit = 1)
    {
        return Application::getInstance()->storage->getCrud()->get(self::getEntityName(), $criteria, $limit);
    }
    
    public static function getAll($criteria = [], $limit)
    {
        return Application::getInstance()->storage->getCrud()->get(self::getEntityName(), $criteria, $limit);
    }
    
    public function save()
    {
        if(self::getOne(['id' => $this->id])){
            return Application::getInstance()->storage->getCrud()->update(self::getEntityName(),
                    /* old value */ ['id' => $this->id],
                    /* new value */ ['id' => $this->id, 'name' => $this->name, 'email' => $this->email, 'password' => $this->password]); 
        } else {
            return Application::getInstance()->storage->getCrud()->set(self::getEntityName(), ['id' => rand(10, 10000), 'name' => $this->name, 'email' => $this->email, 'password' => $this->password]);
        }
    }
    
    public function delete()
    {
        if(self::getOne(['id' => $this->id])){
            return Application::getInstance()->storage->getCrud()->delete(self::getEntityName(),
                    /* del value */ ['id' => $this->id]); 
        } else {
            echo "You have not user whith this ID = ".$this->id;
        }
    }
}