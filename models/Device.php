<?php

namespace models;

class Device
{
    public $id;
    
    public $user_id;
    
    public $info;
    
    public $ip;
    
    public $created_at;
}