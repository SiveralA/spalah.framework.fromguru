<?php

return [
    'id' => 'spalah_framework',
    'baseDir' => __DIR__ . DIRECTORY_SEPARATOR .'..',
    'controllersNamespace' => "\\controllers",
    'baseController' => 'site',
    'errorAction' => 'site/error',
    'storage' => [
        'class' => 'core\storage\FileConnector',
        'path' => __DIR__ . DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR . 'fstorage'
    ],
    'request' => [
        'class' => 'core\components\Request'
    ]
];