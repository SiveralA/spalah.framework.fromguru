<?php

namespace core\base;

class WebController
{
    public $defaultAction = 'index';
    
    public function runAction($name)
    {
        /**
         * @TODO DZ
         *  обработать ошибку вызова не существующего действия
         *  с помощью создания исключения (Exception)
         */
        if(empty($name)){
            $this->{'action'.ucfirst($this->defaultAction)}();
        }else{
            $this->{'action'.ucfirst($name)}();
        }
    }
}