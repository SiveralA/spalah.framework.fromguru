<?php

namespace core\components;

use core\helpers\Inflector;

/**
 * @property string $uri
*/
class Request
{
    protected $uri;

    public function __construct()
    {
        $this->uri = $_SERVER['REQUEST_URI'];
    }

    /**
     * @param string $uri
     * @return string
    */
    public function getControllerNamespace($uri = null)
    {
        $arr = $this->createQueryArray($uri);
        
        $arrCount = count($arr);
        if($arrCount <= 2) {
            return !empty($arr[0]) ? "\\".Inflector::id2camel($arr[0]) : '';
        } else {
            $namespace = '';
            for($i = 0; $i < ($arrCount -1); ++$i){
                if($i == ($arrCount -2)){
                    $namespace .= "\\".Inflector::id2camel($arr[$i]);
                }else{
                    $namespace .= "\\".$arr[$i];
                }                
            }    
            return $namespace;
        }
    }

    /**
     * @return boolean
    */
    public function hasQueryString()
    {
        return (boolean)!empty(ltrim($this->uri, "/"));
    }

    /**
     * @param string $uri
     * @return string|null
     */
    public function getControllerAction($uri = null)
    {
        $arr = $this->createQueryArray($uri);
        
        $arrCount = count($arr);
        if($arrCount >= 2) {
            return !empty($arr[$arrCount-1]) ? $arr[$arrCount-1] : null;
        }
        return null;
    }

    /**
     * @param string $str
     * @return array
    */
    protected function queryStringToArray($str)
    {
        return explode('/', ltrim($str, "/"));
    }

    /**
     * @param string $uri
     * @return array
     */
    protected function createQueryArray($uri = null)
    {
        if($uri)
            return $this->queryStringToArray($uri);
        
        return $this->queryStringToArray($this->uri);        
    }
}