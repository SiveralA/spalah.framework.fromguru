<?php

namespace core\validators;

/**
 * @property ValidatorsInterface $strategy
 */
class Review
{
    private $strategy;

    protected static $aliases = [
        'string' => 'core\validators\StringReview',
        'number' => 'core\validators\NumberReview',
        'array' => 'core\validators\ArrayReview',
    ];
    
    public function __construct(ValidatorsInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * @param mixed
     * @return boolean
    */
    public function validate($value)
    {
        return $this->strategy->validate($value);
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->strategy->message();
    }

    /**
     * @param string $value
     * @param array $rules
     * If error empty, then value check all rules
     * @return array 
    */
    public static function check($value, array $rules = [])
    {
        $errors = [];
        foreach ($rules as $rule) {
            if(!empty(self::$aliases[$rule])){
                /** @var $strategy ValidatorsInterface */
                $strategy = new self::$aliases[$rule];
                $review = new self($strategy);
                if(!$review->validate($value))
                    $errors[] = $review->getError();
            }
        }
        
        return $errors;
    }
}