<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 20.04.17
 * Time: 18:19
 */

namespace core\validators;


interface ValidatorsInterface
{
    /**
     * @param string|integer|array|resource|null|float $value
     * @return boolean
    */
    public function validate($value);

    /**
     * @return string
    */
    public function message();
}