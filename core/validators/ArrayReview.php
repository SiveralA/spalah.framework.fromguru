<?php

namespace core\validators;

class ArrayReview implements ValidatorsInterface
{
    public function validate($value)
    {
        return is_array($value);
    }

    public function message()
    {
        return 'Value is not array type';
    }
}