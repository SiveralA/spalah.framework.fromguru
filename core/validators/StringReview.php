<?php

namespace core\validators;

class StringReview implements ValidatorsInterface
{
    public function validate($value)
    {
        return is_string($value);
    }

    public function message()
    {
        return 'Value is not string type';
    }
}