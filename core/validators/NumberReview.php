<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 20.04.17
 * Time: 18:24
 */

namespace core\validators;


class NumberReview implements ValidatorsInterface
{
    public function validate($value)
    {
        return is_numeric($value);
    }

    public function message()
    {
        return 'Value is not number type';
    }

}