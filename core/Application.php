<?php

namespace core;

use core\exceptions\InvalidAction;
use core\exceptions\InvalidApplicationConfig;
use core\exceptions\InvalidControllerClass;
use core\validators\Review;

/**
 * @property \core\storage\FileConnector $storage
 * @property \core\components\Request $request
*/
class Application
{
    private static $instance;

    public $id;

    public $baseDir;

    public $controllersNamespace;

    public $baseController;
    
    public $errorAction;

    public $storage;

    public $request;

    /**
     * @param array $config
    */
    public function __construct(array $config = [])
    {
        $this->defineApplicationConstant();
        if(APP_DEBUG) {
            $this->validateConfig($config);
        }
        $this->launchApplication($config);
        self::$instance = $this;
    }

    /**
     * @void
     */
    private function defineApplicationConstant()
    {
        if(!defined('APP_DEBUG')) {
            define('APP_DEBUG', true);
        }
    }

    /**
     * @return  Application
    */
    public static function getInstance()
    {
        return self::$instance;
    }

    /**
     * @param array $config
     * @throws \Exception
    */
    protected function validateConfig(array $config)
    {
        $errors = [];
        $requiredMessage = "Must Have params = ";
        if(!empty($config['id'])) {
            $err = Review::check($config['id'], ['string']);
            if($err)
                $errors['id'] = $err;
        }else{
            $errors['id'] = $requiredMessage.' `id`';
        }

        if(!empty($config['baseDir'])) {
            $err = Review::check($config['baseDir'], ['string']);
            if($err)
                $errors['baseDir'] = $err;
        }else{
            $errors['baseDir'] = $requiredMessage.' `baseDir`';
        }

        if(!empty($config['controllersNamespace'])) {
            $err = Review::check($config['controllersNamespace'], ['string']);
            if($err)
                $errors['controllersNamespace'] = $err;
        }else{
            $errors['controllersNamespace'] = $requiredMessage.' `controllersNamespace`';
        }

        if(!empty($config['baseController'])) {
            $err = Review::check($config['baseController'], ['string']);
            if($err)
                $errors['baseController'] = $err;
        }else{
            $errors['baseController'] = $requiredMessage.' `baseController`';
        }

        if(!empty($config['errorAction'])) {
            $err = Review::check($config['errorAction'], ['string']);
            if($err)
                $errors['errorAction'] = $err;
        }else{
            $errors['errorAction'] = $requiredMessage.' `errorAction`';
        }

        if(!empty($config['storage'])) {
            $err = Review::check($config['storage'], ['array']);
            if($err)
                $errors['storage'] = $err;
        }else{
            $errors['storage'] = $requiredMessage.' `storage`';
        }

        if(!empty($config['request'])) {
            $err = Review::check($config['request'], ['array']);
            if($err)
                $errors['request'] = $err;
        }else{
            $errors['request'] = $requiredMessage.' `request`';
        }

        if($errors) {
            throw new InvalidApplicationConfig(json_encode($errors), 69);
        }
    }

    /**
     * @param array $config
     * @throws \Exception
     */
    protected function launchApplication(array $config)
    {
        foreach ($config as $key => $param) {
            if(is_array($param)) {
                if(!empty($param['class'])){
                    $newArray = $param;
                    unset($newArray['class']);
                    $obj = new $param['class']($newArray);
                    $this->{$key} = $obj;
                }
            }else{
                $this->{$key} = $param;
            }
        }
    }

    /**
     * @throws \Exception
    */
    public function run()
    {
        try{
            $this->initController();
        } catch (InvalidControllerClass $e){
            $this->errorActionLaunch();
        } catch (InvalidAction $e){
            $this->errorActionLaunch();
        } catch (\Exception $e) {
            if(APP_DEBUG)
                throw $e;
        }
    }

    /**
     * @void
    */
    protected function initController()
    {
        $controllerLauncher = $this->request->hasQueryString() ? null : $this->baseController;
        $controllerNamespace = $this->request->getControllerNamespace($controllerLauncher);
        $controllerNamespace = $this->getFullControllerNamespace($controllerNamespace);
        if(class_exists($controllerNamespace)) {
            /** @var $controller \core\base\WebController */
            $controller = new $controllerNamespace();
            $controller->runAction($this->request->getControllerAction($controllerLauncher));
        } else {
            throw new InvalidControllerClass("Not found controller class `" . $controllerLauncher . "`");
        }
    }

    /**
     * @param string $shortName
     * @return string
    */
    protected function getFullControllerNamespace($shortName)
    {
        return $controllerNamespace = $this->controllersNamespace . $shortName . 'Controller';
    }

    /**
     * @return void
    */
    protected function errorActionLaunch()
    {
        if($this->errorAction) {
            $controllerNamespace = $this->request->getControllerNamespace($this->errorAction);
            $controllerNamespace = $this->getFullControllerNamespace($controllerNamespace);
            /** @var $controller \core\base\WebController */
            $controller = new $controllerNamespace;
            $controller->runAction($this->request->getControllerAction($this->errorAction));
        }
    }
}