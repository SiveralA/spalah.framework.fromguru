<?php

namespace core\storage;

abstract class Connector
{
    final public function __construct(array $config = [])
    {
        foreach ($config as $name => $value) {
            $this->{$name} = $value;
        }
        $this->init();
    }
    
    public function init()
    {
        
    }

    abstract public function create();
    
    abstract public function close();
    
    abstract public function isActive();
}