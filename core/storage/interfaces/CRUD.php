<?php

namespace core\storage\interfaces;

interface CRUD {
    
    /**
     * @return array
    */
    public function get($nameEntity, array $criteria = [], $limit);
    
    /**
     * @return boolean
    */
    public function set($nameEntity, array $criteria);
    
    /**
     * @return boolean
    */
    public function update($nameEntity, array $criteria, array $update_params);
    
    /**
     * @return boolean
    */
    public function delete($nameEntity, array $criteria);
}