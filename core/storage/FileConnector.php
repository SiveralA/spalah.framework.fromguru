<?php

namespace core\storage;

use core\storage\ext\FileCrud;

class FileConnector extends Connector
{
    public $path;

    protected $_crud;

    public function init()
    {
        $this->_crud = new FileCrud($this);
    }

    /**
     * @return FileCrud
    */
    public function getCrud()
    {
        return $this->_crud;
    }

    /**
     * @return $this
    */
    public function create()
    {
        return $this;
    }

    /**
     * @return boolean
    */
    public function close()
    {
        return true;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return true;
    }

}