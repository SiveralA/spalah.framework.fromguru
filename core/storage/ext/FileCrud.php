<?php
namespace core\storage\ext;

use core\storage\interfaces\CRUD;

use core\storage\FileConnector;

/**
 * @property FileConnector $connect
*/
class FileCrud implements CRUD
{
    protected $connect;

    public function readFile ($nameEntity) {
        $file = $this->connect->path . DIRECTORY_SEPARATOR . $nameEntity.'.json';
        if(is_file($file)){
            $content = file_get_contents($this->connect->path . DIRECTORY_SEPARATOR . $nameEntity.'.json');
            $content = json_decode($content,TRUE);
        }else{
            $content = [];
        }
        return array ($content);
    }


    public function __construct(FileConnector $connect)
    {
        $this->connect = $connect;
    }

    public function get($nameEntity, array $criteria = [], $limit)
    {
        $file = $this->connect->path . DIRECTORY_SEPARATOR . $nameEntity.'.json';
        if(is_file($file)){
            $content = file_get_contents($this->connect->path . DIRECTORY_SEPARATOR . $nameEntity.'.json');
            $content = json_decode($content, true);
            // @TODO доработать функци-анальность по лимиту выдаваемой информации - DONE
            if(is_array($content)) {
                if(empty($criteria)){
                    return $content;
                }else{
                    $base_result = array_filter($content, function ($content) use ($criteria) {
                        /**
                         * @TODO реализовать поиск по масиву с использованием критерием в переменной $criteria
                        */
                        /**
                         * DONE
                         * Search by transmitted key and value in this key
                        */
                        foreach ($criteria as $key => $value) {
                            return in_array($criteria[$key], $content);
                        }
                        
                    });
                    for ($i=1;$i<=$limit;$i++) {
                        $result[$i] = $base_result[$i];
                    }
                    return $result;
                }
            }
        }
        return [];
    }

    public function set($nameEntity, array $criteria)
    {
        list ($content) = $this->readFile($nameEntity);
        $content[] = $criteria;
        $content = json_encode($content);
        file_put_contents($this->connect->path . DIRECTORY_SEPARATOR . $nameEntity.'.json', $content);
    }

    public function update($nameEntity, array $criteria, array $update_params)
    {
        /**
         * @TODO Создать процесс обновление данных по условиям
        */
        list ($content) = $this->readFile($nameEntity);
        foreach ($content as $key => $value) {
            if ($content[$key]['id'] == $update_params['id']) {
                $updated_content[$key] = $update_params;
            } else {
                $updated_content[$key] = $content[$key];
            }
        }
        $updated_content = json_encode($updated_content,TRUE);
        file_put_contents($this->connect->path . DIRECTORY_SEPARATOR . $nameEntity.'.json', $updated_content);
        
    }

    public function delete($nameEntity, array $criteria)
    {
        /**
         * @TODO Создать процесс удаление данных по условиям
         */
        list ($content) = $this->readFile($nameEntity);
        foreach ($content as $key => $value) {
            if ($content[$key]['id'] != $criteria['id']) $updated_content[$key] = $content[$key];
        }
        $updated_content = json_encode($updated_content,TRUE);
        file_put_contents($this->connect->path . DIRECTORY_SEPARATOR . $nameEntity.'.json', $updated_content);
    echo 'Just now, you DELETE user with ID = ' . $criteria['id'];
    }

}