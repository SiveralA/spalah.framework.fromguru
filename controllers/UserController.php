<?php
/**
 * Created by PhpStorm.
 * User: pudchdenis
 * Date: 01.04.17
 * Time: 17:40
 */

namespace controllers;

use core\base\WebController;
use models\User;

class UserController extends WebController
{
    public function actionIndex()
    {
        echo '/** class user action index */<br>';
    }
    // Ввод ()активация загрузки в базу (файл)
    public function actionCreate()
    {
        $user = new User();
        $user->name = "Roman";
        $user->email = "roof555@i.ua";
        $user->created_at = time();
        $user->password = "ASDqwe123";
        $user->save();
        echo 'Just now, you SAVE new user - ' . $user->name;
    }
    // Обновление данных по ID
    public function actionUpdate()
    {
        $user = new User();
        $user->id = 1114; // Изменяемый (ввводимый ID пользователя)
        $user->name = "Dmitriy";
        $user->email = "root@i.ua";
        $user->created_at = time();
        $user->password = "87654321";
        $user->save();
    echo 'Just now, you UPDATE user with ID = ' . $user->id;
    }
    // Удаление данных по ID
    public function actionDelete()
    {
        $user = new User();
        $user->id = 9404; // Удаляемый (ввводимый ID пользователя)
        $user->delete();
    }
    
    public function actionGet()
    {
        $user = new User();
        echo "<pre>";
        var_dump($user->getAll(['id'=>3711],2));
        echo "</pre>";
    }
}